<?php

namespace Db;

use PDO;
use \MongoDB\Driver\Manager;
use \MongoDB\Driver\Command;
class Connection
{
    private static ?PDO $pdo;
    private static ?\MongoDB\Driver\Manager $mongo;

    public static function get(): PDO {
        if (!isset(self::$pdo)) {
            //postgres
            $dbName = getenv('DB_NAME');
            $dbUser = getenv('DB_USER');
            $dbPassword = getenv('DB_PASSWORD');

            self::$pdo = new PDO("pgsql:host=postgres user=postgres dbname=spotifiie password=postgres");
        }

        return self::$pdo;
    }
    

    public static function getMongo(): \MongoDB\Driver\Manager {
        if (!isset(self::$mongo)) {
            self::$mongo = new \MongoDB\Driver\Manager("mongodb://ykj:ykj@mongo:27017/users");
            // var_dump(self::$mongo);
        }
        return self::$mongo;
    }
}
