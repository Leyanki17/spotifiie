<?php
    require_once('./src/Db/Connection.php');
    require_once('./src/Router.php');
    require_once('./src/model/ChansonStoragePgsql.php');
    require_once('./src/model/AccountStoragePgsql.php');
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    
    /**
     * Connexion à la base de donnéé 
     */
    try{
        $bd= \Db\Connection::get();
        $bd->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }catch(PDOEXCEPTION $e){
        die($e->getMessage());
    }


    try{
        $user= \Db\Connection::getMongo();
    }catch(EXCEPTION $e){
        die($e->getMessage());
    }
    
   
    
    // instanciation de notre router
    $router= new Router();
    
    // var_dump($router);
    
    // passe la base de donnée en paramétre de main

    $router->main(new \model\ChansonStoragePgsql($bd),new \model\AccountStoragePgsql($bd));
     
?>
